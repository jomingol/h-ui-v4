# H-ui.v4 前端框架

## 一、介绍
采用Gulp作为构建工具，css采用scss编写，js依赖jQuery框架。用到的构建组件请查看package.json配置文件，可自行去gulp官网查询插件的api。

```
del                 删除文件
gulp-autoprefixer   设置浏览器版本自动处理浏览器前缀
gulp-clean-css      压缩css文件
gulp-concat         文件合并
gulp-connect        启动本地服务
gulp-htmlmin        压缩html文件
gulp-jshint         js代码检查
gulp-rename         重命名
sass                解析sass
gulp-sass           解析sass
gulp-uglify         压缩javascript文件
```

## 二、目录结构

```
├─ node_modules     node包文件
├─ src 源码目录
├─ build 编译打包后目录
```

```
引入的第三方插件
├── jquery                jQuery类库（v1.9.1）
├── Hui-iconfont          阿里图标字体库（H-ui定制）
├── jquery.SuperSlide      幻灯片组件
├── Validform              表单验证插件
├── jquery.validation      表单验证插件
├── My97DatePicker        日期插件
├── datatables            表格插件
├── nprogress              进度条插件
├── layer                  layer弹出层插件
├── laypage                laypage 翻页插件
├── html5shiv.js          html5插件，让低版本IE支持html5元素
├── DD_belatedPNG_0.0.8a-min.js    解决IE6png透明
├── swfobject.js          Flash插件
├── expressInstall.swf    检查flash插件
├── respond.min.js        让IE兼容media
```

## 三、安装

```
npm install
```

友情提示：npm是node下的包管理工具，node官网自行下载安装。

## 四、启动

```
gulp start
```

当你看到以下文字提示，恭喜你服务启动成功。

```
[16:05:03] Starting server...
[16:05:03] Server started http://localhost:9003
[16:05:03] LiveReload started on port 35729
[16:05:03] Running server
```

你在使用的时候，只需要把build里的文件拷贝到你的项目下就可以使用。

如果你想修改H-ui的源码，请直接修改src目录下的文件即可。

**使用构建工具的好处：**

1. 就是源码和编译压缩后的代码分离。
2. 自由定制你的UI，可以有选择性的压缩你需要的模块，用不上的js，css模块可以删除，最大限度的缩小你的项目体积。


## 五、其他命令

```
gulp cleanAll 清空build目录下文件
gulp cleanCss 清空src下的css目录
gulp scss  将scss生成css，放到src/css目录下
gulp css  将scss文件合并，解析成css文件，压缩，放到build/h-ui/css 目录下
gulp js 合并压缩js，放到build/h-ui/js 目录下
gulp image 将image放到build/h-ui/images 目录下
gulp lib 将lib放到build/lib 目录下
gulp watch 监听文件变化
gulp build 打包
```