/* =======================================================================
 * jQuery.Huispinner.js v2.1.2 微调器
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2017.06.26
 *
 * Copyright 2017 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * ========================================================================*/
!function($) {	
	$.fn.Huispinner = function(options, callback) {
		var defaults = {
			value : 1,
			minValue : 1,
			maxValue : 999,
			dis : 1,
		}
		var options = $.extend(defaults, options);
		var keyCodes = {
			up : 38,
			down : 40
		}

		this.each(function() {
			var that = $(this);
			var str = '<div class="hui-input-number">'
					+ '<a class="decrease" href="javascript:void(0)"><i>-</i></a>'
					+ '<input class="amount hui-input-text" value="'
					+ options.value + '" autocomplete="off">'
					+ '<a class="increase" href="javascript:void(0)"><i>+</i></a>'
					+ '</div>';
			that.append(str);

			var input = that.find(".hui-input-text"),
				decrease = that.find(".decrease"),
				increase = that.find(".increase"),
				value = parseInt(input.val());

			if (value <= options.minValue) {
				decrease.addClass("disabled");
				increase.removeClass("disabled");
			}
			if (value >= options.maxValue) {
				decrease.removeClass("disabled");
				increase.addClass("disabled");
			}

			// 输入框失去焦点
			input.on('blur', function() {
				var v = $(this).val();
				v = v.replace(/[^\d]/g, "");
				v = v.replace(/\b(0+)/gi, "");

				if (v != "") {
					if (v > options.minValue && v < options.maxValue) {
						input.val(v)
						increase.removeClass("disabled");
						decrease.removeClass("disabled");
					} else {
						if (v <= options.minValue) {
							input.val(options.minValue);
							decrease.addClass("disabled");
							increase.removeClass("disabled");
						} else {
							input.val(options.maxValue);
							decrease.removeClass("disabled");
							increase.addClass("disabled");
						}
					}
				} else {
					$(this).val(options.minValue);
					decrease.addClass("disabled");
					increase.removeClass("disabled");
				}
				if (callback) {
					callback(input.val());
				}
			});

			// 上下方向键
			input.on("keydown", function(e) {
				var evt = e || window.event;
				if (evt.keyCode === keyCodes.up) {
					decrease.trigger("click");
					evt.returnValue = false;
				}
				if (evt.keyCode === keyCodes.down) {
					increase.trigger("click");
					evt.returnValue = false;
				}
			});

			// 减
			decrease.on('click', function() {
				var goodsCount = parseInt(input.val());
				input.val(goodsCount <= options.minValue
						? options.minValue
						: goodsCount - options.dis);
						increase.removeClass("disabled");
				if (input.val() <= options.minValue) {
					input.val(options.minValue)
					decrease.addClass("disabled");
				}
				if (callback) {
					callback(input.val());
				}
			});

			// 加
			increase.on('click', function() {
				var goodsCount = parseInt(input.val());
				input.val(goodsCount >= options.maxValue
						? options.maxValue
						: goodsCount + options.dis);
				decrease.removeClass("disabled");

				if (input.val() >= options.maxValue) {
					input.val(options.maxValue);
					increase.addClass("disabled");
				}
				if (callback) {
					callback(input.val());
				}
			});
		});
	}
}(window.jQuery);