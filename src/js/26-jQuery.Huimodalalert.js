/* =======================================================================
 * jQuery.Huimodalalert.js v1.0 提示对话框
 * ========================================================================*/
!function ($) {
  $.fn.Huimodalalert = function (options, callback) {
    var defaults = {
      type: 'info',
      btn: ['确定'],
      content:'弹窗内容',
      speed: "0",
      area: ['400', 'auto'],
    };
    var options = $.extend(defaults, options);
    this.each(function () {
      var that = $(this);
      var w= 0,h=0,t=0,l=0;
      if (options.area[0]=='auto'){
        w ='400px';
        l = -(400 / 2) + 'px';
      }else{
        w = options.area[0] + 'px';
        l = -(options.area[0] / 2) + 'px';
      }
      if (options.area[1] == 'auto') {
        h = 'auto';
      } else {
        h = options.area[1] + 'px';
      }
      var modalClassName = '';
      var modalIcon = '<i class="hui-iconfont mr-5">&#xe6e0;</i>';
      if(options.type=='success') {
        modalClassName = 'hui-modal-alert-success';
        modalIcon = '<i class="hui-iconfont mr-5">&#xe6e1;</i>';
      }
      if(options.type=='error') {
        modalClassName = 'hui-modal-alert-error';
        modalIcon = '<i class="hui-iconfont mr-5">&#xe60b;</i>';
      }
      var htmlstr =
      '<div id="hui-modal-alert" class="hui-modal hui-modal-alert radius" style="width:' + w + ';height:' + h + ';margin-left:' + l +'">' +
        '<div class="hui-modal-alert-info">'+ modalIcon + options.content + '</div>' +
        '<div class="hui-modal-footer">'+
          '<button class="hui-btn hui-btn-primary radius">' + options.btn[0]+'</button>'+
        '</div>' +
      '</div>'+
      '<div id="hui-modal-mask" class="hui-modal-mask"></div>';

      if ($("#hui-modal-alert").length > 0) {
        $("#hui-modal-alert,#hui-modal-mask").remove();
      }
      if (options.speed==0){
        $(document.body).addClass("hui-modal-open").append(htmlstr);
        $("#hui-modal-alert").addClass(modalClassName).fadeIn();
      }else{
        $(document.body).addClass("hui-modal-open").append(htmlstr);
        $("#hui-modal-alert").find(".hui-modal-footer").remove();
        $("#hui-modal-alert").addClass(modalClassName).fadeIn();
        setTimeout(function(){
          $("#hui-modal-alert").fadeOut("normal",function () {
            huimodalhide();
          });
        }, options.speed);
      }

      var button = that.find(".hui-modal-footer .hui-btn");
      button.on("click",function(){
        $("#hui-modal-alert").fadeOut("normal", function () {
          huimodalhide();
        });
      });

      function huimodalhide(){
        $("#hui-modal-alert,#hui-modal-mask").remove();
        $(document.body).removeClass("hui-modal-open");
        if (callback) {
          callback();
        }
      }
    });
  }
}(window.jQuery);