/* =======================================================================
 * jQuery.Huialert.js alert
 * ========================================================================*/
!function($) {
	$.Huialert = function() {
		$('.hui-alert i').Huihover();
		$(document).on("click",".hui-alert i",function() {
			var Huialert = $(this).parents(".hui-alert");
			Huialert.fadeOut("normal",function() {
				Huialert.remove();
			});
		});
	}
	$.Huialert();
} (window.jQuery);