/* =======================================================================
 * jQuery.Huitotop.js v3.0 返回顶部
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2022.04.26
 *
 * Copyright 2017 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * ========================================================================*/
!function($) {
	//bottom 距离底部高度
	$.Huitotop = function(bottom){		
		if(!bottom){
			bottom = 60;
		}
		var str ='<a href="javascript:void(0)" class="hui-tools-right hui-totop hui-iconfont" title="返回顶部" alt="返回顶部" style="display:none;bottom:'+bottom+'px">&#xe684;</a>';
		$(str).appendTo($('body')).click(function() {
			$("html, body").animate({
				scrollTop: 0
			},
			120);
		});
		var backToTopFun = function(){
			var st = $(document).scrollTop();
			var winh = $(window).height();
			if(st> 0){
				$(".hui-totop").show();
			}else{
				$(".hui-totop").hide();
			}
			/*IE6下的定位*/
			if (!window.XMLHttpRequest) {
				$(".hui-totop").css("top", st + winh - 166);
			}
			
		}		
		$(window).on("scroll",backToTopFun);	
	}
} (window.jQuery);