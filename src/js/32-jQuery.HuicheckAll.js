/* =======================================================================
 * jQuery.HuicheckAll.js v1.0 全选
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2019.07.01
 *
 * Copyright 2019 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * ========================================================================*/
// 全选与反选 2019.7.1 14:28 @guojunhui
!function($) {
  $.fn.HuicheckAll = function(options,callback) {
    var defaults = {
      checkboxAll: 'thead input[type="checkbox"]',
      checkbox: 'tbody input[type="checkbox"]'
    }
    var options = $.extend(defaults, options);
    this.each(function(){
      var that = $(this);
      var checkboxAll = that.find(options.checkboxAll);
      var checkbox = that.find(options.checkbox);

      checkboxAll.on("click",function(){
        var isChecked = checkboxAll.prop("checked");
        checkbox.prop("checked", isChecked);
        var _Num = 0,checkedArr = [];
        checkbox.each(function(){
          if($(this).prop("checked")) {
            checkedArr.push($(this).val());
            _Num++;
          }
        });
        var checkedInfo = {
          Number: _Num,
          checkedArr: checkedArr
        }
        if(callback){
          callback(checkedInfo);
        }
      });

      checkbox.on("click",function(){
        var allLength = checkbox.length;
        var checkedLength = checkbox.prop("checked").length;
        allLength == checkedLength ? checkboxAll.prop("checked",true) : checkboxAll.prop("checked",false);
        var _Num = 0,checkedArr = [];
        checkbox.each(function(){
          if($(this).prop("checked")) {
            checkedArr.push($(this).val());
            _Num++;
          }
        });
        var checkedInfo = {
          Number: _Num,
          checkedArr: checkedArr
        }
        if(callback){
          callback(checkedInfo);
        }
      });
    });
  }
} (window.jQuery);